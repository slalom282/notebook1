﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Telbook
{
    class Program
    {
        public static int id = 1;
        public static Dictionary<int, Contact> Contacts = new Dictionary<int, Contact>();
        static void Main(string[] args)
        {
            Console.WriteLine("Записная книжка 1.0");
            while (true)
            {
                Console.WriteLine("Выберите действие:");
                Console.WriteLine("    1 - Добавить в записную книжку запись;");
                Console.WriteLine("    2 - Изменить в записной книжке запись;");
                Console.WriteLine("    3 - Удалить в записной книжке запись;");
                Console.WriteLine("    4 - Показать все записи в записной книжке;");
                Console.WriteLine("    5 - Завершить работу в записной книжке.");
                string v = Console.ReadLine();
                if (v == "1")
                {
                    Console.WriteLine("Введите имя, фамилию, номер и страну контакта, в разных строках.");
                    Contacts.Add(id, new Contact(id, Console.ReadLine(), Console.ReadLine(), Int32.Parse(Console.ReadLine()), Console.ReadLine()));
                    id++;
                    continue;
                }

                if (v == "2")
                {
                    Console.WriteLine("Введите id записи, котрую хотите редактировать");
                    int c = Int32.Parse(Console.ReadLine());
                    Console.WriteLine("К какому полю вы хотите обратиться: Фамилия, Имя, Отчество, Номер телефона, Страна, Дата рождения, " +
                        "Органиация, Должность, Прочие заметки");
                    Console.WriteLine("Текст, который вы хотите написать в это поле. Если вы хотите его удалить - напишите Отсутствует");

                    Edit(c, Console.ReadLine(), Console.ReadLine());
                    continue;
                }

                if (v == "3")
                {
                    Console.WriteLine("Введите id записи, котрую хотите удалить");
                    Contacts.Remove(Int32.Parse(Console.ReadLine()));
                    Console.WriteLine("Запись удалена.");
                    continue;
                }

                if (v == "4")
                {
                    if (Contacts.Count > 0)
                    {
                        foreach (KeyValuePair<int, Contact> contact in Contacts)
                        {
                            Console.WriteLine(contact.ToString());
                        };
                        continue;
                    }
                    else
                    {
                        Console.WriteLine("Записная книга пуста");
                        continue;
                    }
                }

                if (v == "5")
                {
                    Console.WriteLine("До новых встреч!");
                    Thread.Sleep(1000);
                    break;
                }
                else
                    Console.WriteLine("Не знаю такого. Давайте еще раз ппробуем");
            }
        }



        public static void Edit(int id, string pole, string value)
        {
            if (pole == "Фамилия")
            {
                Contacts[id].Famil = value;
            }
            if (pole == "Имя")
            {
                Contacts[id].Name = value;
            }
            if (pole == "Отчество")
            {
                Contacts[id].Father = value;
            }
            if (pole == "Телефон")
            {
                Contacts[id].Phone = Int32.Parse(value);
            }
            if (pole == "Страна")
            {
                Contacts[id].Country = value;
            }
            if (pole == "День рождения")
            {
                Contacts[id].Birthday = value;
            }
            if (pole == "Организация")
            {
                Contacts[id].Organisation = value;
            }
            if (pole == "Должность")
            {
                Contacts[id].Work = value;
            }
            if (pole == "Прочие заметки")
            {
                Contacts[id].Another = value;
            }
        }
    }
}

class Contact
    {
        private int id;
        private string name;
        private string famil;
        private string father;
        private int phone;
        private string country;
        private string birthday;
        private string organisation;
        private string work;
        private string another;

        public string Name { get => name; set => name = value; }
        public string Famil { get => famil; set => famil = value; }
        public string Father { get => father; set => father = value; }
        public int Phone { get => phone; set => phone = value; }
        public string Country { get => country; set => country = value; }
        public string Birthday { get => birthday; set => birthday = value; }
        public string Organisation { get => organisation; set => organisation = value; }
        public string Work { get => work; set => work = value; }
        public string Another { get => another; set => another = value; }

        public Contact(int id, string name, string famil, int phone, string country)
        {
            this.id = id;
            this.Name = name;
            this.Famil = famil;
            this.Phone = phone;
            this.Country = country;
        }
        public override string ToString()
        {
            return ("ID: " + id + ". Фамилия контакта " + Famil + ". Имя контакта " + Name + ". Телефон: " + Phone);
        }
        public string ToStringFull()
        {
            return ("ID: " + id + ". Фамилия контакта " + Famil + ". Имя контакта " + Name
                + ". Отчество контакта" + Father == null ? " Отсутствует" : Father + ". Телефон: " +
                Phone + ". Страна" + Country + ". День рождения " + Birthday == null ? " Отсутствует" : Birthday +
                ". Организация" + Organisation == null ? " Отсутствует" : Organisation +
                ". Должность" + Work == null ? " Отсутствует" : Work +
                ". Прочие заметки" + Another == null ? " Отсутствует" : Another);
        }
}
